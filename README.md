# node-docker

A thingy to make some Nodejs run in the Dockers, on the typey-typey white 
text on black background such that you can run bits of Nodejs on the terminal. 

## Setting Up
1. `docker-compose build --no-cache`
2. `docker-compose up -d`
3. `docker exec -it node-docker /bin/bash`
4. Do some node
   - For example, run `tower-of-babel`
   - Host machine can change `./nodecode` and the change will be reflected _live_ in Dockers, at the original working directory, such that your favourite IDE can be used to do some Javascripts.
   - `exit`
5. `docker-compose down`